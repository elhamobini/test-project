import createCache from "@emotion/cache";
import { CacheProvider } from "@emotion/react";
import AddIcon from "@mui/icons-material/Add";
import {
  Box,
  Button,
  Collapse,
  Container,
  Paper,
  Stack,
  Typography,
} from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";
import { jssPreset, StylesProvider } from "@mui/styles";
import { create } from "jss";
import rtl from "jss-rtl";
import type { NextPage } from "next";
import Head from "next/head";
import { useEffect, useState } from "react";
import { prefixer } from "stylis";
import rtlPlugin from "stylis-plugin-rtl";
import Header from "../src/components/Header";
import AddLinkForm from "../src/sections/AddLinkForm";
import LinkItem from "../src/sections/LinkItem";
import theme from "../src/theme/theme";
import api from "./api/link";

// Create rtl cache
const cacheRtl = createCache({
  key: "muirtl",
  stylisPlugins: [prefixer, rtlPlugin],
});

const jss = create({
  plugins: [...jssPreset().plugins, rtl()],
});

const Home: NextPage = () => {
  const [open, setOpen] = useState<boolean>(false);
  const [links, setLinks] = useState([]);
  const retrieveLinks = async () => {
    const response = await api.get("/links");
    return response.data;
  };
  const removeLinkHandler = async (id: number) => {
    await api.delete(`/links/${id}`);
    const newList = links.filter((link) => link?.id !== id);
    setLinks(newList);
  };

  useEffect(() => {
    const getAllLinks = async () => {
      const allLinks = await retrieveLinks();
      setLinks(allLinks);
    };

    getAllLinks();
  }, []);
  return (
    <>
      <style jsx global>
        {`
          body {
            margin: 0px;
            color: rgb(255, 255, 255);
            font-family: iranyekan, Arial, sans-serif;
            font-weight: 400;
            font-size: 0.857143rem;
            line-height: 1.5;
            background-color: rgb(22, 27, 37);
          }
        `}
      </style>
      <Head>
        <title>Test App</title>
        <meta
          name="keywords"
          content="front-end react developer test interview , A test for the job interview"
        />
      </Head>
      <CacheProvider value={cacheRtl}>
        <ThemeProvider theme={theme}>
          <StylesProvider jss={jss}>
            <Container
              dir="rtl"
              maxWidth="md"
              sx={{
                marginTop: "40px",
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                minWidth: "300px",
              }}
            >
              <Box mt={"32px"} width="100%">
                <Header />
                <Paper
                  sx={{
                    background: "rgb(33, 43, 53)",
                    marginTop: "48px",
                    padding: "24px",
                    borderRadius: "16px",
                  }}
                >
                  <Box>
                    <Typography
                      variant="caption"
                      sx={{ color: "rgb(121, 131, 142)" }}
                      display={"block"}
                    >
                      مسیرهای ارتباطی
                    </Typography>
                    <Button
                      size="small"
                      color="secondary"
                      sx={{ mt: "16px" }}
                      startIcon={<AddIcon fontSize="small" />}
                      onClick={() => setOpen(true)}
                      disabled={open}
                    >
                      افزودن مسیر ارتباطی
                    </Button>
                  </Box>
                  {/*Form*/}
                  <Stack spacing={"16px"}>
                    <Collapse in={open}>
                      <AddLinkForm
                        setOpen={setOpen}
                        isEdit={false}
                        links={links}
                        setLinks={setLinks}
                      />
                    </Collapse>
                    {/* <LinkList /> */}
                    <Stack spacing={2}>
                      {links?.map((link: any, i: number) => (
                        <LinkItem
                          key={i}
                          id={link?.id}
                          link={link?.link}
                          type={link?.type}
                          removeLinkHandler={removeLinkHandler}
                          links={links}
                          setLinks={setLinks}
                          // editLinkHandler={editLinkHandler}
                        />
                      ))}
                    </Stack>
                  </Stack>
                </Paper>
              </Box>
            </Container>
          </StylesProvider>
        </ThemeProvider>
      </CacheProvider>
    </>
  );
};

export default Home;

// export const getStaticProps = async () => {
//   const response = await api.get("/links");
//   const links = await response.data;
//   return {
//     props: {
//       links,
//     },
//   };
// };
