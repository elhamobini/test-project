import { createTheme } from "@mui/material/styles";
import palette from "./palette";

const theme = createTheme({
  // palette: isLight ? palette.light : palette.dark,
  palette: palette.dark,
  // direction: themeDirection,
  direction: "rtl",
  typography: {
    h1: {},
    h2: {},
    h3: {},
    h4: {},
    h5: {},
    h6: {},
    body1: {
      fontWeight: 500,
    },
    body2: {
      fontWeight: 400,
    },

    subtitle1: {
      fontWeight: 200,
    },
    subtitle2: {},
    button: {
      fontWeight: 500,
      color: "#ffffff !important",
    },
  },
});
export default theme;
