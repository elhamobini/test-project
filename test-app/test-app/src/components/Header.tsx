import {
  Box,
  Breadcrumbs,
  Button,
  Link,
  Stack,
  Typography,
} from "@mui/material";

import CircleIcon from "@mui/icons-material/Circle";
import CircleRoundedIcon from "@mui/icons-material/CircleRounded";

function Header() {
  function handleClick(event: React.MouseEvent<HTMLDivElement, MouseEvent>) {
    event.preventDefault();
    console.info("You clicked a breadcrumb.");
  }
  return (
    <>
      <Stack
        direction={"row"}
        alignItems="center"
        justifyContent="space-between"
        width={"100%"}
      >
        <Typography variant="h6" mb={"8px"}>
          تنظیمات کاربری
        </Typography>

        <Box gap={"16px"} sx={{ display: "flex", alignItems: "center" }}>
          <Button
            size="medium"
            sx={{
              transition: "all 0.5s ease 0s",
            }}
          >
            English
          </Button>
          <Button
            size="medium"
            sx={{
              transition: "all 0.5s ease 0s",
            }}
            color="secondary"
          >
            فارسی
          </Button>
        </Box>
      </Stack>
      {/* BreadCrumbs */}
      <Box role="presentation" onClick={handleClick}>
        <Breadcrumbs
          sx={{ color: "rgb(121, 131, 142)" }}
          aria-label="breadcrumb"
          separator={
            <CircleIcon
              sx={{ color: "rgb(121, 131, 142)", fontSize: "10px" }}
            />
          }
        >
          <Link href="/" underline="hover" color={"rgb(121, 131, 142)"}>
            خانه
          </Link>
          <Link
            href="/material-ui/getting-started/installation/"
            underline="hover"
            color={"rgb(121, 131, 142)"}
          >
            کاربر
          </Link>
          <Link href="/" underline="hover" color={"rgb(121, 131, 142)"}>
            تنظیمات کاربری
          </Link>
        </Breadcrumbs>
      </Box>
    </>
  );
}

export default Header;
