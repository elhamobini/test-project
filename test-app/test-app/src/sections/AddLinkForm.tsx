import {
  Autocomplete,
  Box,
  Button,
  Grid,
  Paper,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import api from "../../pages/api/link";

function AddLinkForm(props: {
  setOpen?: any;
  isEdit?: boolean;
  links?: any;
  setLinks?: any;
  id?: number;
  link?: string;
  type?: string | undefined;
}) {
  const { setOpen, isEdit, links, setLinks, id, link, type } = props;
  const [socialType, setSocialType] = useState([]);

  //.....................form

  const {
    setValue,
    register,
    reset,
    formState: { errors },
    handleSubmit,
  } = useForm({
    mode: "onChange",
    defaultValues: {
      link: "",
      type: "",
    },
  });

  const onSubmit = async (data: any) => {
    console.log("data", data);

    const request = {
      id: Math.floor(Math.random() * 100),
      ...data,
    };

    const response = await api.post("/links", request);
    if (response) {
      setLinks([...links, response.data]);
      reset();
    }
  };

  //.....................
  const handelCancel = () => {
    setOpen(false);
    reset();
  };

  const socialLinksType = async () => {
    const response = await api.get("/socialLinks");
    return response.data;
  };

  useEffect(() => {
    const getSocialLinksType = async () => {
      const socialLinks = await socialLinksType();
      setSocialType(socialLinks);
    };

    getSocialLinksType();
  }, []);

  return (
    <>
      <Paper
        sx={{
          marginTop: "16px",
          backgroundColor: "Grey.500",
          borderRadius: "16px",
          padding: "16px",
          color: "#fff",
          backgroundImage:
            "linear-gradient(rgba(255, 255, 255, 0.05), rgba(255, 255, 255, 0.05))",
        }}
      >
        <Box mb={"8px"}>
          <Typography variant="subtitle2">
            {isEdit ? `ویرایش مسیر ارتباطی ${type}` : "تایید مسیر ارتباطی"}
          </Typography>
        </Box>

        <form onSubmit={handleSubmit(onSubmit)}>
          <Grid container spacing={"8px"}>
            <Grid item xs={12} md={4}>
              <Stack>
                <Autocomplete
                  disablePortal
                  id="combo-box-demo"
                  options={socialType}
                  renderInput={(params) => (
                    <TextField
                      color="secondary"
                      type="type"
                      {...params}
                      label="نوع"
                      {...register("type", {
                        required: {
                          value: true,
                          message: "وارد کردن این فیلد اجباری است.",
                        },
                      })}
                    />
                  )}
                />

                <Typography color="error" fontSize={13} mr={2} ml={2}>
                  {errors.type?.message}
                </Typography>
              </Stack>
            </Grid>
            <Grid item xs={12} md={8}>
              <Stack>
                <TextField
                  type="link"
                  color="secondary"
                  label="لینک"
                  dir="ltr"
                  sx={{
                    width: "100%",
                    "& .muirtl-1v0t1ns-MuiFormLabel-root-MuiInputLabel-root": {
                      color: "#747c82",
                    },
                  }}
                  {...register("link", {
                    required: {
                      value: true,
                      message: "وارد کردن این فیلد اجباری است.",
                    },
                    pattern: {
                      value:
                        /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/,
                      message:
                        "محتویات این فیلد باید از جنس آدرس اینترنتی باشد",
                    },
                  })}
                />
                <Typography color="error" fontSize={13} mr={2} ml={2}>
                  {errors.link?.message}
                </Typography>
              </Stack>
            </Grid>
          </Grid>

          <Box
            mt={"16px"}
            gap={"8px"}
            display={"flex"}
            justifyContent={"right"}
          >
            <Button
              onClick={handelCancel}
              color="secondary"
              variant="outlined"
              size="small"
            >
              انصراف
            </Button>
            <Button
              color="secondary"
              variant="contained"
              type="submit"
              size="small"
            >
              تایید مسیر ارتباطی
            </Button>
          </Box>
        </form>
      </Paper>
    </>
  );
}

export default AddLinkForm;
