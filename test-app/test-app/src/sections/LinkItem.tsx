import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import EditIcon from "@mui/icons-material/Edit";
import TwitterIcon from "@mui/icons-material/Twitter";
import {
  Autocomplete,
  Button,
  Collapse,
  Grid,
  Link,
  Modal,
  Paper,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import { useEffect, useState } from "react";
import theme from "../theme/theme";
import api from "../../pages/api/link";
import { useForm } from "react-hook-form";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  borderRadius: 2,
  boxShadow: 24,
  p: 4,
};
//.............................................
function LinkItem(props: {
  id: number;
  link?: string;
  type?: string;
  removeLinkHandler: (id: number) => void;
  links?: any;
  setLinks?: any;
  // editLinkHandler: (id: number) => void;
}) {
  const { id, link, type, removeLinkHandler, links, setLinks } = props;
  const [open, setOpen] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [confirmValue, setConfirmValue] = useState<string>("");
  const [isDisabled, setIsDisabled] = useState<boolean>(true);
  const isEdit = true;
  const handleOpen = () => setOpenModal(true);
  const handleClose = () => setOpenModal(false);
  const [idLink, setIdLink] = useState<number>();
  const [socialType, setSocialType] = useState([]);

  const socialLinksType = async () => {
    const response = await api.get("/socialLinks");
    return response.data;
  };

  useEffect(() => {
    const getSocialLinksType = async () => {
      const socialLinks = await socialLinksType();
      setSocialType(socialLinks);
    };

    getSocialLinksType();
  }, []);

  const {
    setValue,
    register,
    reset,
    formState: { errors },
    handleSubmit,
  } = useForm({
    mode: "onChange",
    defaultValues: {
      link: "",
      type: "",
    },
  });

  const onSubmit = async (data: any) => {
    await api.put(`/links/${id}`, data);
    let index = links.findIndex((item: any) => item.id === id);
    const updateList = links.splice(index, 1, {
      id: id,
      link: data.link,
      type: data.type,
    });
    setLinks([...links]);
    setOpen(false);
    reset();
    
  };

  //.....................
  const handelCancel = () => {
    reset();
    setOpen(false);
  };

  return (
    <Stack
      alignItems="center"
      p={2}
      bgcolor={theme.palette.background.neutral}
      borderRadius={4}
    >
      <Stack width={"100%"}>
        <Box
          alignItems="center"
          display={"flex"}
          justifyContent="space-between"
        >
          <Stack direction={"row"} alignItems="center" spacing={1}>
            <Stack direction={"row"} alignItems="center" mr={1} spacing={1}>
              <TwitterIcon />
              <Typography variant="body1">{type}</Typography>
            </Stack>
            <Stack direction={"row"} alignItems="center" spacing={1}>
              <Typography variant="caption">لینک :</Typography>

              <Link href="#" color={"secondary"}>
                {link}
              </Link>
            </Stack>
          </Stack>
          <Stack direction={"row"} alignItems="center" spacing={1}>
            <Button
              color="secondary"
              onClick={() => {
                setOpen(true);
                setIdLink(id);
                setValue("type", `${type}`);
                setValue("link", `${link}`);
              }}
              disabled={open}
              startIcon={<EditIcon fontSize="small" />}
            >
              ویرایش
            </Button>
            <Button
              color="warning"
              // onClick={() => removeLinkHandler(id)}
              onClick={handleOpen}
            >
              <DeleteForeverIcon
                color="warning"
                sx={{ fontSize: 14, margin: "0px 5px 0px 5px " }}
              />
              حذف
            </Button>
            <Modal
              open={openModal}
              onClose={handleClose}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
            >
              <Stack sx={style} spacing={1}>
                <Typography id="modal-modal-title" variant="h6">
                  آیا از تصمیم خود مطمئن هستید؟
                </Typography>
                <Typography
                  id="modal-modal-description"
                  variant="body1"
                  sx={{ mt: 2 }}
                >
                  برای حذف مسیر ارتباطی twitter لطفا تایید را وارد نمایید.
                </Typography>
                <TextField
                  type="text"
                  placeholder="تایید*"
                  onChange={(e) => {
                    setConfirmValue(e.target.value);
                    if (e.target.value === "تایید") {
                      setIsDisabled(false);
                    } else {
                      setIsDisabled(true);
                    }
                  }}
                />
                <Box gap={"8px"}>
                  <Button
                    variant="outlined"
                    color="secondary"
                    size="small"
                    onClick={handleClose}
                  >
                    انصراف
                  </Button>
                  <Button
                    variant="outlined"
                    color="primary"
                    size="small"
                    onClick={() => {
                      removeLinkHandler(id);
                      handleClose();
                    }}
                    disabled={isDisabled}
                  >
                    حذف
                  </Button>
                </Box>
              </Stack>
            </Modal>
          </Stack>
        </Box>
        <Collapse in={open}>
          <Paper
            sx={{
              marginTop: "16px",
              backgroundColor: "Grey.500",
              borderRadius: "16px",
              padding: "16px",
              color: "#fff",
              backgroundImage:
                "linear-gradient(rgba(255, 255, 255, 0.05), rgba(255, 255, 255, 0.05))",
            }}
          >
            <Box mb={"8px"}>
              <Typography variant="subtitle2">
                {`ویرایش مسیر ارتباطی ${type}`}
              </Typography>
            </Box>

            <form onSubmit={handleSubmit(onSubmit)}>
              <Grid container spacing={"8px"}>
                <Grid item xs={12} md={4}>
                  <Stack>
                    <Autocomplete
                      disablePortal
                      id="combo-box-demo"
                      options={socialType}
                      color="secondary"
                      renderInput={(params) => (
                        <TextField
                          error={errors.type ? true : false}
                          {...params}
                          label="نوع"
                          {...register("type", { required: true })}
                        />
                      )}
                    />

                    <Typography color="error" fontSize={13} mr={2} ml={2}>
                      {errors.type && "وارد کردن این فیلد اجباری است."}
                    </Typography>
                  </Stack>
                </Grid>
                <Grid item xs={12} md={8}>
                  <Stack>
                    <TextField
                      autoComplete="off"
                      error={errors.link ? true : false}
                      color="secondary"
                      label="لینک"
                      dir="ltr"
                      {...register("link", {
                        required: true,
                        pattern: {
                          value:
                            /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
                          message:
                            "محتویات این فیلد باید از جنس آدرس اینترنتی باشد.",
                        },
                      })}
                    />

                    {errors.link?.message ? (
                      <Typography color="error" fontSize={13} mr={2} ml={2}>
                        {errors.link?.message && errors.link?.message}
                      </Typography>
                    ) : (
                      <Typography color="error" fontSize={13} mr={2} ml={2}>
                        {errors.link && "وارد کردن این فیلد اجباری است."}
                      </Typography>
                    )}
                  </Stack>
                </Grid>
              </Grid>

              <Box
                mt={"16px"}
                gap={"8px"}
                display={"flex"}
                justifyContent={"right"}
              >
                <Button
                  onClick={handelCancel}
                  color="secondary"
                  variant="outlined"
                  size="small"
                >
                  انصراف
                </Button>
                <Button
                  color="secondary"
                  variant="contained"
                  type="submit"
                  size="small"
                >
                  {`ویرایش مسیر ارتباطی ${type}`}
                </Button>
              </Box>
            </form>
          </Paper>
        </Collapse>
      </Stack>
    </Stack>
  );
}

export default LinkItem;
